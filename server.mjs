import { fileURLToPath } from "url";
import { dirname } from "path";
import express from "express";
import path from "path";
import bodyParser from "body-parser";
import shelljs from "shelljs";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const app = express();
const PORT = 1234;

// Configure Express to use EJS
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use("/public", express.static(path.join(__dirname, "public")));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.render("index");
});

app.post("/exec", (req, res) => {
  const reqBody = req.body;

  const command = String(reqBody.command).trim();

  if (!command) {
    res.status(400);
    res.send("Command is empty!");
  } else {
    const result = execCommand(command);
    res.send("$> " + result);
  }
});

function execCommand(command) {
  return shelljs.exec(command);
}

app.listen(PORT, () => {
  console.log(`Server running on http://localhost:${PORT}`);
});