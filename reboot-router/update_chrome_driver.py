import os.path
import re as regex
import shutil
import zipfile
from re import RegexFlag

import requests
from selenium.common import SessionNotCreatedException

DRIVER__COMPRESSED__FILE_NAME = "D:\\FileSetup\\chromedriver-win64.zip"
DRIVER__FOLDER_NAME = "D:\\FileSetup\\chromedriver-win64"
DRIVER__EXECUTION_NAME = "chromedriver.exe"

CHROME_PAGE__DRIVER_DASHBOARD_PAGE = "https://googlechromelabs.github.io/chrome-for-testing/"
CHROME_PAGE__DRIVER_DOWNLOAD_URL = "https://storage.googleapis.com/chrome-for-testing-public/$chrome_version$/win64/chromedriver-win64.zip"


def update(ex: SessionNotCreatedException):
    print(f"updating chrome driver.... from: {ex.msg}")

    err_msg = ex.msg
    matched = regex.search("Current browser version is (.+?) with binary path", err_msg)

    current_chrome_version = matched.group(1)
    print("Current version obtained: ", current_chrome_version)
    crawl_driver__chrome(current_chrome_version)


def obtain_current_version_url_from_download_page(version: str, page_content: str):
    download_driver_base_url = CHROME_PAGE__DRIVER_DOWNLOAD_URL

    short_version = version[0:version.index(".")]
    version_pattern = f"<section class=status-ok id=stable><h2>Stable</h2><p>Version: <code>{short_version}(.*?)</code>"
    matched_version = regex.search(version_pattern,
                                   page_content,
                                   RegexFlag.M | RegexFlag.I)

    remain_version_number = matched_version.group(1)
    current_version_number = short_version + remain_version_number;
    print("download - matched version: ", current_version_number)

    return download_driver_base_url.replace("$chrome_version$", current_version_number)


def crawl_driver__chrome(version: str):
    with requests.get(CHROME_PAGE__DRIVER_DASHBOARD_PAGE) as http_resp:
        current_driver_link = obtain_current_version_url_from_download_page(version, http_resp.content.decode("utf-8"))

        print("current downloading driver link: ", current_driver_link)
        clear_old_driver()
        download_driver_to_local(current_driver_link)
        unzip_driver_in_local()


def clear_old_driver():
    if (os.path.isfile(DRIVER__COMPRESSED__FILE_NAME)):
        os.remove(DRIVER__COMPRESSED__FILE_NAME)

    if (os.path.isdir(DRIVER__FOLDER_NAME)):
        shutil.rmtree(DRIVER__FOLDER_NAME)


def download_driver_to_local(link: str):
    print(f"Downloading chrome driver from link: " + link)

    with requests.get(link) as http_resp:
        driver_data = http_resp.content

        with open(DRIVER__COMPRESSED__FILE_NAME, "wb") as f:
            f.write(driver_data)
            print("saved driver in ", f)


def unzip_driver_in_local():
    with zipfile.ZipFile(DRIVER__COMPRESSED__FILE_NAME, "r") as zf:
        zf.extractall(DRIVER__FOLDER_NAME)

    if not os.path.isfile(os.path.join(DRIVER__FOLDER_NAME, DRIVER__EXECUTION_NAME)):
        for item_folder in os.listdir(DRIVER__FOLDER_NAME):
            if (os.path.isdir(os.path.join(DRIVER__FOLDER_NAME, item_folder))):
                for i_item_file_name in os.listdir(os.path.join(DRIVER__FOLDER_NAME, item_folder)):
                    if (os.path.isfile(os.path.join(DRIVER__FOLDER_NAME, item_folder, i_item_file_name))):
                        print(f"moving... {i_item_file_name} from {os.path.join(DRIVER__FOLDER_NAME, item_folder)} to {DRIVER__FOLDER_NAME}")
                        shutil.move(
                            os.path.join(DRIVER__FOLDER_NAME, item_folder, i_item_file_name),
                            DRIVER__FOLDER_NAME
                        )

                shutil.rmtree(os.path.join(DRIVER__FOLDER_NAME, item_folder))
