import json
import os
import sys
from datetime import date

from selenium import webdriver
from selenium.common.exceptions import SessionNotCreatedException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait

import update_chrome_driver as ucd

CONFIG__LINK_ROUTER = "linkRouter"
CONFIG__PASSWORD = "password"
CONFIG__LAST_REBOOT_DATE = "lastRebootDate"

CURR_DIR_PATH = os.path.abspath(sys.argv[0])


def load_properties():
    with open(CURR_DIR_PATH + "/data.json") as f:
        global config_data
        config_data = json.load(f)

        print("Config Properties: ")
        print(config_data)


def save_config_properties():
    with open(CURR_DIR_PATH + "/data.json", "w") as f:
        str_config_data = json.dumps(config_data, indent=2)
        f.write(str_config_data)


def wait_element(element_css_selector):
    WebDriverWait(driver, timeout=15).until(lambda d: d.find_element(By.CSS_SELECTOR, element_css_selector))


def open_page():
    driver.get(config_data[CONFIG__LINK_ROUTER])


def login():
    wait_element("#submitOperation .btn.btn-link")
    pass_inp = driver.find_element(By.CSS_SELECTOR, "input[name='Password']")
    btn_submit = driver.find_element(By.CSS_SELECTOR, "#submitOperation .btn.btn-link")

    pass_inp.send_keys(config_data[CONFIG__PASSWORD])
    btn_submit.click()


def go_to_tools_page():
    wait_element("#submitOperation #submit_ok")

    btn_advanced_submit = driver.find_element(By.CSS_SELECTOR, "#submitOperation #submit_ok")
    btn_advanced_submit.click()

    wait_element("#navbar-nav #system")

    tab_tool_page = driver.find_element(By.CSS_SELECTOR, "#navbar-nav #system")
    tab_tool_page.click()


def reboot_router():
    wait_element("iframe#main_iframe")
    tool_frame = driver.find_element(By.CSS_SELECTOR, "iframe#main_iframe")

    driver.switch_to.frame(tool_frame)
    wait_element("input#rebootBtn")
    btn_reboot = driver.find_element(By.CSS_SELECTOR, "input#rebootBtn")
    btn_reboot.click()

    alert = WebDriverWait(driver, timeout=15).until(expected_conditions.alert_is_present())
    alert = driver.switch_to.alert
    alert.accept()


def close_page():
    driver.close()


def run():
    load_properties()

    today = date.today().strftime("%Y-%m-%d")

    print("Today = ", today)

    do_running(today, False)
    # ucd.update(SessionNotCreatedException("Current browser version is 123.0 with binary path"))


def do_running(today: str, updateDriverWhenFail: bool):
    try:
        if (today != config_data[CONFIG__LAST_REBOOT_DATE]):
            print("start app")
            global driver
            driver = webdriver.Chrome()
            print("browser name: ", driver.name)
            print("capabilities: ", driver.capabilities.get(driver.name))
            open_page()
            login()
            go_to_tools_page()
            reboot_router()
            close_page()
            print("close app")

            config_data[CONFIG__LAST_REBOOT_DATE] = today
            save_config_properties()
        else:
            print("Not time to reset router")
    except SessionNotCreatedException as ex:
        print("SessionNotCreatedException has occurred: ", ex.msg)

        if not updateDriverWhenFail:
            ucd.update(ex)
            do_running(today, True)
        else:
            print("Has updated driver, but still happening exception, please review.")